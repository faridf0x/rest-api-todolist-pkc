<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
require APPPATH . 'libraries/REST_Controller.php';


class Catatan_Subjek extends REST_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('Pegawai_model');
    }

    public function index_get()
    {
        $access_token = $this->get('access_token');

        $token_status = $this->check_token($access_token);

        if($token_status){
            $id = $this->get('EMPLOYEEID');
            
            $this->getSubjek($id);
        }else{
            $this->response([
                   'status' => FALSE,
                   'message' => 'Token Auth Failed'
                ], REST_Controller::HTTP_BAD_REQUEST);
        }        

        
    }    

    public function getSubjek($employeeid)
    {
      if ($employeeid == null)
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Login OR tahun were not found',
                'data' => null
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
        else
        {
            $catatan = $this->Pegawai_model->getSubjekCatatanParentUser($employeeid);
            $j = 0;
            for ($i = 0; $i < count($catatan); $i++) {
                $data[$j]['CATATANID'] = $catatan[$j]['CATATANID'];
                $data[$j]['SUBJEK'] = $catatan[$j]['SUBJEK'];
                $j++;
            }
        
            $this->response([
              'status' => true,
              'message' => 'data found',
              'data' => $data
            ], REST_Controller::HTTP_OK);   
        }
    }

    public function getCatatanParent($employeeid, $tahun)
    {
      if ($employeeid == null OR $tahun == null)
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Login OR tahun were not found'
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
        else
        {
            $catatan = $this->Pegawai_model->getCatatanParentUser($employeeid, $tahun);
            return $catatan;   
        }
    }

    public function index_post()
    {

        
    }

    public function index_put()
    {
     	$access_token = $this->put('access_token');

        $token_status = $this->check_token($access_token);

        if($token_status){
        	$id = $this->put('CATATANID');
        	$parentcatatan = $this->put('PARENTCATATAN');
	        $data = [
	            'PROGRESS' => $this->put('PROGRESS')
	        ];

	        if($parentcatatan == 0){
	        	if($this->Pegawai_model->putProgressCatatanPersonal($data, $id) > 0){
		            $this->response([
		                'status' => true,
		                'message' => 'Progress Updated!'
		            ], REST_Controller::HTTP_OK);   
		        }else{
		            $this->response([
		                'status' => false,
		                'message' => 'Failed to update Progress!'
		            ], REST_Controller::HTTP_BAD_REQUEST);  
		        }
	        }else{
	        	$where = [
	        		'EMPLOYEEID' => $this->put('EMPLOYEEID'),
	            	'PARENTCATATANID' => $id
	        	];
	        	if($this->Pegawai_model->putProgressCatatanChild($data, $where) > 0){
		            $this->response([
		                'status' => true,
		                'message' => 'Progress Updated!'
		            ], REST_Controller::HTTP_OK);   
		        }else{
		            $this->response([
		                'status' => false,
		                'message' => 'Failed to update Progress!'
		            ], REST_Controller::HTTP_BAD_REQUEST);  
		        }
	        }	
        }else{
            $this->response([
                   'status' => FALSE,
                   'message' => 'Token Auth Failed'
                ], REST_Controller::HTTP_BAD_REQUEST);
        }        
    }

    public function index_delete()
    {

    }

    public function check_token($access_token)
    {
        $where = array(
        'key' => $access_token
      );

      // get data from database based on user input
      $token = $this->Pegawai_model->cekToken($where);

      if(isset($token)){
        // Token accepted
        return true;
      }else{
        // Token failed
        return false;

      }
    }

}
