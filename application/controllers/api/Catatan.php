<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
require APPPATH . 'libraries/REST_Controller.php';


class Catatan extends REST_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('Pegawai_model');
    }

    public function uploadAttachment_post(){
            $config['upload_path'] = './assets/images';
            $config['allowed_types'] = 'gif|jpg|png|pdf';
            $config['max_size']  = '2400';
            $config['max_width']  = '1024';
            $config['max_height']  = '768';
            $config['width']= 10;
            $config['height']= 10;
            $config['file_name']  = 'AT_';
            
            $this->load->library('upload', $config);
            
            if ( ! $this->upload->do_upload('profile_image')){
                $error = array('error' => $this->upload->display_errors());
                var_dump($error);
            }
            else{
                $data = array('upload_data' => $this->upload->data());
                
                $this->response([
                    'status' => true,
                    'message' => 'Report canceled!',
                    'data' =>  base_url().'/assets/images/'.$this->upload->data('file_name')
                ], REST_Controller::HTTP_OK);   
            }
    }

    public function accTask_put()
    {
        $access_token = $this->put('access_token');
        $token_status = $this->check_token($access_token);

        if($token_status){
            

            $data = [
                'TANGGALPENGERJAANMULAI' => $this->put('TANGGALPENGERJAANMULAI'),
                'FLAGPROGRESS' => 'ONPROGRESS'
            ];
            if($this->put('PARENTCATATAN') == 1){
                $where = [
                  'ID' => $this->put('ID')
                ];
                if($this->Pegawai_model->putAccCatatanUnit($data, $where) > 0){
                    $this->response([
                        'status' => true,
                        'message' => 'Task Progress Updated!',
                        'data' => null
                    ], REST_Controller::HTTP_OK);   
                }else{
                    $this->response([
                        'status' => false,
                        'message' => 'Failed to update Task Progress!',
                        'data' => null
                    ], REST_Controller::HTTP_BAD_REQUEST);  
                }
            }else{
                $where = [
                  'CATATANID' => $this->put('ID')
                ];
                if($this->Pegawai_model->putAccCatatanPersonal($data, $where) > 0){
                    $this->response([
                        'status' => true,
                        'message' => 'Task Progress Updated!',
                        'data' => null
                    ], REST_Controller::HTTP_OK);   
                }else{
                    $this->response([
                        'status' => false,
                        'message' => 'Failed to update Task Progress!',
                        'data' => null
                    ], REST_Controller::HTTP_BAD_REQUEST);  
                }
            }
                
        }else{
            $this->response([
                   'status' => FALSE,
                   'message' => 'Token Auth Failed'
                ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function updateProgress_put()
    {
        $access_token = $this->put('access_token');

        $token_status = $this->check_token($access_token);

        if($token_status){
            $id = $this->put('ID');
            $parentcatatan = $this->put('PARENTCATATAN');
            $data = [
                'PROGRESS' => $this->put('PROGRESS')
            ];
            if($parentcatatan == 0){

                if($this->Pegawai_model->putProgressCatatanPersonal($data, $id) > 0){
                    $this->response([
                        'status' => true,
                        'message' => 'Progress Updated!'
                    ], REST_Controller::HTTP_OK);   
                }else{
                    $this->response([
                        'status' => false,
                        'message' => 'Failed to update Progress!'
                    ], REST_Controller::HTTP_BAD_REQUEST);  
                }
            }else{
                $where = [
                    'EMPLOYEEID' => $this->put('EMPLOYEEID'),
                    'ID' => $id
                ];
                if($this->put('PROGRESS') == '100'){
                    $data = [
                        'PROGRESS' => $this->put('PROGRESS'),
                        'CATATAN' => $this->put('CATATAN'),
                        'ATTACHMENT' => $this->put('ATTACHMENT')
                    ];
                }
                if($this->Pegawai_model->putProgressCatatanChild($data, $where) > 0){
                    $this->response([
                        'status' => true,
                        'message' => 'Progress Updated!'
                    ], REST_Controller::HTTP_OK);   
                }else{
                    $this->response([
                        'status' => false,
                        'message' => 'Failed to update Progress!'
                    ], REST_Controller::HTTP_BAD_REQUEST);  
                }
            }   
        }else{
            $this->response([
                   'status' => FALSE,
                   'message' => 'Token Auth Failed'
                ], REST_Controller::HTTP_BAD_REQUEST);
        }        
    }

    public function cancelReport_put()
    {
        $access_token = $this->put('access_token');
        $token_status = $this->check_token($access_token);

        if($token_status){
            $id = $this->put('ID');
            $data = [
                'PROGRESS' => '99'
            ];

            $where = [
                'ID' => $id
            ];
            if($this->Pegawai_model->putProgressCatatanChild($data, $where) > 0){
                $this->response([
                    'status' => true,
                    'message' => 'Report canceled!'
                ], REST_Controller::HTTP_OK);   
            }else{
                $this->response([
                    'status' => false,
                    'message' => 'Failed to cancel Report!'
                ], REST_Controller::HTTP_BAD_REQUEST);  
            }
        }else{
            $this->response([
                   'status' => FALSE,
                   'message' => 'Token Auth Failed'
                ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function accReport_put()
    {
        $access_token = $this->put('access_token');
        $token_status = $this->check_token($access_token);

        if($token_status){
            $where = [
              'ID' => $this->put('ID')
            ];

            if($this->put('VALIDASI') == 1){
                $data = [
                    'VALIDASIPARENT' => $this->put('VALIDASIPARENT')
                ];
                if($this->Pegawai_model->putValidasiCatatan($data, $where) > 0){
                    $this->response([
                        'status' => true,
                        'message' => 'validation Updated!',
                        'data' => null
                    ], REST_Controller::HTTP_OK);   
                }else{
                    $this->response([
                        'status' => false,
                        'message' => 'Failed to update validation!',
                        'data' => null
                    ], REST_Controller::HTTP_BAD_REQUEST);  
                }
            }else{
                $data = [
                    'VALIDASIPARENT' => $this->put('VALIDASIPARENT'),
                    'COMMENTPARENT' => $this->put('COMMENTPARENT')
                ];
            
                if($this->Pegawai_model->putValidasiCatatan($data, $where) > 0){
                    $where_parent = [
                      'CATATANID' => $this->put('PARENTCATATANID')
                    ];
                    $data_parent = [
                        'TANGGALRENCANASELESAI' => $this->put('TANGGALRENCANASELESAI')
                    ];
                    if($this->Pegawai_model->putValidasiCatatanParent($data_parent, $where_parent) > 0){
                        $this->response([
                            'status' => true,
                            'message' => 'validation Updated!',
                            'data' => null
                        ], REST_Controller::HTTP_OK);   
                    }else{
                        $this->response([
                            'status' => false,
                            'message' => 'Failed to update validation parent',
                            'data' => null
                        ], REST_Controller::HTTP_BAD_REQUEST);  
                    }
                }else{
                    $this->response([
                        'status' => false,
                        'message' => 'Failed to update validation child!',
                        'data' => null
                    ], REST_Controller::HTTP_BAD_REQUEST);  
                }
            }
        }else{
            $this->response([
                   'status' => FALSE,
                   'message' => 'Token Auth Failed'
                ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

 
    public function catatanTodo_get()
    {
        $employeeid = $this->get('EMPLOYEEID');
        $parentcatatan = $this->get('PARENTCATATAN');
        $parentperson = $this->get('PARENTPERSON');
        $tahun = $this->get('TAHUN');
        $access_token = $this->get('access_token');

        $token_status = $this->check_token($access_token);
        if($token_status){
            if($parentcatatan == 0){             
                    $this->getCatatan($employeeid, $tahun, "BELUMDIKERJAKAN");
            }else{
                    if($parentperson == 1){
                        $catatan = $this->getCatatanParent($employeeid, $tahun, "BELUMDIKERJAKAN");    
                        $jml_cat = count($catatan);
                        $j = 0;
                        for ($i = 0; $i < $jml_cat; $i++) {
                            $catatanChild = $this->getCatatanParentClild($catatan[$j]['CATATANID']);
                            $jml_catChild = count($catatanChild);
                            $catatan[$i]{'jml_child'} = $jml_catChild;
                            $catatan[$i]{'CHILDCATATAN'} = $catatanChild;
                            $j++;
                        }
                        $this->response([
                          'status' => true,
                          'message' => 'data found',
                          'jumlah_catatan' => $jml_cat,
                          'data' => $catatan
                        ], REST_Controller::HTTP_OK); 
                    }else{
                        $child_catatan = $this->getCatatanClild($employeeid, $tahun);
                        $catatan2 = []; //variabel catatan inti
                        $j = 0; //variabel index catatan
                        $j2 = 0; //variabel index child catatan
                        $hasil_cek = "tidak";
                        for ($i = 0; $i < count($child_catatan); $i++) {
                            if($catatan2 != null){
                                $z = 0;
                                for ($k = 0; $k < count($catatan2); $k++) {
                                    if ($child_catatan[$j2]['PARENTCATATANID'] == $catatan2[$k]{'CATATANID'}) {
                                        $hasil_cek = "ya";
                                        $z = $k;
                                    }else {
                                        $hasil_cek = "tidak";
                                    }   
                                }
                                if ($hasil_cek == "ya") {
                                    $l = count($catatan2[$z]['CHILDCATATAN']);
                                    $catatan2[$z]['CHILDCATATAN'][$l] = $child_catatan[$j2];
                                }else {
                                    $catatan2[$j] = $this->Pegawai_model->getCatatanChildGetParentUser($child_catatan[$j2]['PARENTCATATANID']);
                                    $catatan2[$j]['CHILDCATATAN'] = $child_catatan[$j2];
                                    $j++;
                                }    
                            }else {
                                $catatan2[$j] = $this->Pegawai_model->getCatatanChildGetParentUser($child_catatan[$j2]['PARENTCATATANID']);
                                $catatan2[$j]['CHILDCATATAN'][0] = $child_catatan[$j2];
                                $j++;
                            }
                            $j2++;   
                        }
                        

                        $this->response([
                          'status' => true,
                          'message' => 'data found',
                          'data' => $catatan2
                        ], REST_Controller::HTTP_OK); 
                    }
            }
        }else {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Token Auth Failed'
                ], REST_Controller::HTTP_OK);
        }
        
    }

    public function catatanOnprogress_get()
    {
        $employeeid = $this->get('EMPLOYEEID');
        $parentcatatan = $this->get('PARENTCATATAN');
        $parentperson = $this->get('PARENTPERSON');
        $tahun = $this->get('TAHUN');
        $access_token = $this->get('access_token');

        $token_status = $this->check_token($access_token);
        if($token_status){
            if($parentcatatan == 0){             
                    $this->getCatatan($employeeid, $tahun, "ONPROGRESS");
            }else{
                    if($parentperson == 1){
                        $catatan = $this->getCatatanParent($employeeid, $tahun, "ONPROGRESS");    
                        $jml_cat = count($catatan);
                        $j = 0;
                        for ($i = 0; $i < $jml_cat; $i++) {
                            $catatanChild = $this->getCatatanParentClild($catatan[$j]['CATATANID']);
                            $jml_catChild = count($catatanChild);
                            $catatan[$i]{'jml_child'} = $jml_catChild;
                            $catatan[$i]{'CHILDCATATAN'} = $catatanChild;
                            $j++;
                        }
                        $this->response([
                          'status' => true,
                          'message' => 'data found',
                          'jumlah_catatan' => $jml_cat,
                          'data' => $catatan
                        ], REST_Controller::HTTP_OK); 
                    }else{
                        $child_catatan = $this->getCatatanClild($employeeid, $tahun);
                        $catatan2 = []; //variabel catatan inti
                        $j = 0; //variabel index catatan
                        $j2 = 0; //variabel index child catatan
                        $hasil_cek = "tidak";
                        for ($i = 0; $i < count($child_catatan); $i++) {
                            if($catatan2 != null){
                                $z = 0;
                                for ($k = 0; $k < count($catatan2); $k++) {
                                    if ($child_catatan[$j2]['PARENTCATATANID'] == $catatan2[$k]{'CATATANID'}) {
                                        $hasil_cek = "ya";
                                        $z = $k;
                                    }else {
                                        $hasil_cek = "tidak";
                                    }   
                                }
                                if ($hasil_cek == "ya") {
                                    $l = count($catatan2[$z]['CHILDCATATAN']);
                                    $catatan2[$z]['CHILDCATATAN'][$l] = $child_catatan[$j2];
                                }else {
                                    $catatan2[$j] = $this->Pegawai_model->getCatatanChildGetParentUser($child_catatan[$j2]['PARENTCATATANID']);
                                    $catatan2[$j]['CHILDCATATAN'] = $child_catatan[$j2];
                                    $j++;
                                }    
                            }else {
                                $catatan2[$j] = $this->Pegawai_model->getCatatanChildGetParentUser($child_catatan[$j2]['PARENTCATATANID']);
                                $catatan2[$j]['CHILDCATATAN'][0] = $child_catatan[$j2];
                                $j++;
                            }
                            $j2++;   
                        }
                        

                        $this->response([
                          'status' => true,
                          'message' => 'data found',
                          'data' => $catatan2
                        ], REST_Controller::HTTP_OK); 
                    }
            }
        }else {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Token Auth Failed'
                ], REST_Controller::HTTP_OK);
        }
        
    }
  
    public function catatandDone_get()
    {
        $employeeid = $this->get('EMPLOYEEID');
        $parentcatatan = $this->get('PARENTCATATAN');
        $parentperson = $this->get('PARENTPERSON');
        $tahun = $this->get('TAHUN');
        $access_token = $this->get('access_token');

        $token_status = $this->check_token($access_token);
        if($token_status){
            if($parentcatatan == 0){             
                    $this->getCatatan($employeeid, $tahun, "DONE");
            }else{
                    if($parentperson == 1){
                        $catatan = $this->getCatatanParent($employeeid, $tahun, "DONE");    
                        $jml_cat = count($catatan);
                        $j = 0;
                        for ($i = 0; $i < $jml_cat; $i++) {
                            $catatanChild = $this->getCatatanParentClild($catatan[$j]['CATATANID']);
                            $jml_catChild = count($catatanChild);
                            $catatan[$i]{'jml_child'} = $jml_catChild;
                            $catatan[$i]{'CHILDCATATAN'} = $catatanChild;
                            $j++;
                        }
                        $this->response([
                          'status' => true,
                          'message' => 'data found',
                          'jumlah_catatan' => $jml_cat,
                          'data' => $catatan
                        ], REST_Controller::HTTP_OK); 
                    }else{
                        $child_catatan = $this->getCatatanClild($employeeid, $tahun);
                        $catatan2 = []; //variabel catatan inti
                        $j = 0; //variabel index catatan
                        $j2 = 0; //variabel index child catatan
                        $hasil_cek = "tidak";
                        for ($i = 0; $i < count($child_catatan); $i++) {
                            if($catatan2 != null){
                                $z = 0;
                                for ($k = 0; $k < count($catatan2); $k++) {
                                    if ($child_catatan[$j2]['PARENTCATATANID'] == $catatan2[$k]{'CATATANID'}) {
                                        $hasil_cek = "ya";
                                        $z = $k;
                                    }else {
                                        $hasil_cek = "tidak";
                                    }   
                                }
                                if ($hasil_cek == "ya") {
                                    $l = count($catatan2[$z]['CHILDCATATAN']);
                                    $catatan2[$z]['CHILDCATATAN'][$l] = $child_catatan[$j2];
                                }else {
                                    $catatan2[$j] = $this->Pegawai_model->getCatatanChildGetParentUser($child_catatan[$j2]['PARENTCATATANID']);
                                    $catatan2[$j]['CHILDCATATAN'] = $child_catatan[$j2];
                                    $j++;
                                }    
                            }else {
                                $catatan2[$j] = $this->Pegawai_model->getCatatanChildGetParentUser($child_catatan[$j2]['PARENTCATATANID']);
                                $catatan2[$j]['CHILDCATATAN'][0] = $child_catatan[$j2];
                                $j++;
                            }
                            $j2++;   
                        }
                        

                        $this->response([
                          'status' => true,
                          'message' => 'data found',
                          'data' => $catatan2
                        ], REST_Controller::HTTP_OK); 
                    }
            }
        }else {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Token Auth Failed'
                ], REST_Controller::HTTP_OK);
        }
        
    }


    public function getCatatan($employeeid, $tahun, $flagprogress = NULL)
    {
      if ($employeeid == null OR $tahun == null)
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Login OR tahun were not found'
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
        else
        {
            if($flagprogress == NULL){
                $catatan = $this->Pegawai_model->getCatatanUserAll($employeeid, $tahun);
            }else{
                $catatan = $this->Pegawai_model->getCatatanUser($employeeid, $tahun, $flagprogress);
            }
            if($catatan == NULL){
                $this->response([
                    'status' => FALSE,
                    'message' => 'Data Kosong'
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code       
            }else {
                $this->response([
                'status' => true,
                'message' => 'data found',
                'data' => $catatan
                ], REST_Controller::HTTP_OK);
            }
        }
    }

    public function getCatatanParent($employeeid, $tahun, $flagprogress = NULL)
    {
      if ($employeeid == null OR $tahun == null)
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Login OR tahun were not found'
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
        else
        {
            if($flagprogress == NULL){
                $catatan = $this->Pegawai_model->getCatatanParentUserAll($employeeid, $tahun);
            }else {
                $catatan = $this->Pegawai_model->getCatatanParentUser($employeeid, $tahun, $flagprogress);
            }
            if($catatan == NULL){
                $this->response([
                    'status' => FALSE,
                    'message' => 'Data Kosong'
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code       
            }else {
                return $catatan;       
            }
            
        }
    }

    public function getCatatanClild($employeeid, $tahun)
    {
      if ($employeeid == null OR $tahun == null)
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Login OR tahun were not found'
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
        else
        {
            return $this->Pegawai_model->getCatatanChildUser($employeeid, $tahun);
        }
    }

    public function getCatatanParentClild($parentcatatanid)
    {
      if ($parentcatatanid == null)
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Login OR tahun were not found'
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
        else
        {
            return $this->Pegawai_model->getCatatanParentChildUser($parentcatatanid);
        }
    }

    public function index_get()
    {
        $employeeid = $this->get('EMPLOYEEID');
        $parentcatatan = $this->get('PARENTCATATAN');
        $parentperson = $this->get('PARENTPERSON');
        $tahun = $this->get('TAHUN');
        $access_token = $this->get('access_token');

        $token_status = $this->check_token($access_token);
        if($token_status){
            if($parentcatatan == 0){             
                    $this->getCatatan($employeeid, $tahun);
            }else{
                    if($parentperson == 1){
                        $catatan = $this->getCatatanParent($employeeid, $tahun);    
                        $jml_cat = count($catatan);
                        $j = 0;
                        for ($i = 0; $i < $jml_cat; $i++) {
                            $catatanChild = $this->getCatatanParentClild($catatan[$j]['CATATANID']);
                            $jml_catChild = count($catatanChild);
                            $catatan[$i]{'jml_child'} = $jml_catChild;
                            $catatan[$i]{'CHILDCATATAN'} = $catatanChild;
                            $j++;
                        }
                        $this->response([
                          'status' => true,
                          'message' => 'data found',
                          'jumlah_catatan' => $jml_cat,
                          'data' => $catatan
                        ], REST_Controller::HTTP_OK); 
                    }else{
                        $child_catatan = $this->getCatatanClild($employeeid, $tahun);
                        $catatan2 = []; //variabel catatan inti
                        $j = 0; //variabel index catatan
                        $j2 = 0; //variabel index child catatan
                        $hasil_cek = "tidak";
                        for ($i = 0; $i < count($child_catatan); $i++) {
                            if($catatan2 != null){
                                $z = 0;
                                for ($k = 0; $k < count($catatan2); $k++) {
                                    if ($child_catatan[$j2]['PARENTCATATANID'] == $catatan2[$k]{'CATATANID'}) {
                                        $hasil_cek = "ya";
                                        $z = $k;
                                    }else {
                                        $hasil_cek = "tidak";
                                    }   
                                }
                                if ($hasil_cek == "ya") {
                                    $l = count($catatan2[$z]['CHILDCATATAN']);
                                    $catatan2[$z]['CHILDCATATAN'][$l] = $child_catatan[$j2];
                                }else {
                                    $catatan2[$j] = $this->Pegawai_model->getCatatanChildGetParentUser($child_catatan[$j2]['PARENTCATATANID']);
                                    $catatan2[$j]['CHILDCATATAN'] = $child_catatan[$j2];
                                    $j++;
                                }    
                            }else {
                                $catatan2[$j] = $this->Pegawai_model->getCatatanChildGetParentUser($child_catatan[$j2]['PARENTCATATANID']);
                                $catatan2[$j]['CHILDCATATAN'][0] = $child_catatan[$j2];
                                $j++;
                            }
                            $j2++;   
                        }
                        

                        $this->response([
                          'status' => true,
                          'message' => 'data found',
                          'data' => $catatan2
                        ], REST_Controller::HTTP_OK); 
                    }
            }
        }else {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Token Auth Failed'
                ], REST_Controller::HTTP_OK);
        }
        
    }    


    public function index_post()
    {
        $access_token = $this->post('access_token');

        $token_status = $this->check_token($access_token);

        if($token_status){
            $ambil_tgl = substr($this->post('TANGGALRENCANAMULAI'), 0, 10);
            if($this->post('PARENTCATATAN') == 0){
                if($this->post('FLAGPRIORITAS') == 1){
                    $hasil_cek_prioritas = $this->check_flagprioritas_personal($ambil_tgl);
                }else {
                    $hasil_cek_prioritas = false;
                }
                

                if($hasil_cek_prioritas){
                    $this->response([
                           'status' => false,
                           'message' => 'Sudah Terdapat Task Prioritas di Tanggal ini'
                    ], REST_Controller::HTTP_BAD_REQUEST);  
                }else{
                    $data = [
                        'EMPLOYEEID' => $this->post('EMPLOYEEID'),
                        'FLAGPRIORITAS' => $this->post('FLAGPRIORITAS'),
                        'FLAGPROGRESS' => $this->post('FLAGPROGRESS'),
                        'SUBJEK' => strtolower($this->post('SUBJEK')),
                        'DESKRIPSI' => $this->post('DESKRIPSI'),
                        'TAHUN' => $this->post('TAHUN'),
                        'TANGGALPEMBUATAN' => $this->post('TANGGALPEMBUATAN'),
                        'TANGGALRENCANAMULAI' => $this->post('TANGGALRENCANAMULAI'),
                        'TANGGALRENCANASELESAI' => $this->post('TANGGALRENCANASELESAI'),
                        'TANGGALPENGERJAANMULAI' => $this->post('TANGGALPENGERJAANMULAI'),
                        'TANGGALPENGERJAANSELESAI' => $this->post('TANGGALPENGERJAANSELESAI'),
                        'PARENTCATATAN' => $this->post('PARENTCATATAN'),
                        'ATTACHMENT' => $this->post('ATTACHMENT')
                    ];

                    if($this->Pegawai_model->postCatatanUser($data)>0){
                        $this->response([
                            'status' => true,
                            'message' => 'Created!'
                        ], REST_Controller::HTTP_CREATED);  
                    }else{
                        $this->response([
                            'status' => false,
                            'message' => 'Failed!'
                        ], REST_Controller::HTTP_BAD_REQUEST);  
                    }
                }
            }else{
                if($this->post('FLAGPRIORITAS') == 1){
                    $hasil_cek_prioritas = $this->check_flagprioritas_unit($ambil_tgl);
                }else {
                    $hasil_cek_prioritas = false;
                }
                

                if($hasil_cek_prioritas){
                    $this->response([
                           'status' => false,
                           'message' => 'Sudah Terdapat Task Prioritas di Tanggal ini'
                    ], REST_Controller::HTTP_BAD_REQUEST);  
                }else{
                    $data = [
                        'EMPLOYEEID' => $this->post('EMPLOYEEID'),
                        'FLAGPRIORITAS' => $this->post('FLAGPRIORITAS'),
                        'FLAGPROGRESS' => $this->post('FLAGPROGRESS'),
                        'SUBJEK' => strtolower($this->post('SUBJEK')),
                        'TAHUN' => $this->post('TAHUN'),
                        'TANGGALPEMBUATAN' => $this->post('TANGGALPEMBUATAN'),
                        'TANGGALRENCANAMULAI' => $this->post('TANGGALRENCANAMULAI'),
                        'TANGGALRENCANASELESAI' => $this->post('TANGGALRENCANASELESAI'),
                        'TANGGALPENGERJAANMULAI' => $this->post('TANGGALPENGERJAANMULAI'),
                        'TANGGALPENGERJAANSELESAI' => $this->post('TANGGALPENGERJAANSELESAI'),
                        'PARENTCATATAN' => $this->post('PARENTCATATAN'), 
                        'COMMENT' => $this->post('COMMENT')
                    ];

                    $jumlah_client = count($this->post('CLIENTID'));
                    $clientid = $this->post('CLIENTID');
                    $employeeid = $this->post('EMPLOYEEID');
                    $subjekchild = $this->post('SUBJEKCHILD');
                    $taskchild = $this->post('TASKCHILD');
                    if($this->post('CATATANID') == null){
                        $parentcatatanid = $this->Pegawai_model->getLastCatatanUser($employeeid);    
                        $parentid = $parentcatatanid['CATATANID'];
                        $parentid = $parentid+1;
                    }else{
                        $parentid = $this->post('CATATANID');
                    }
                    
                    
                    if($this->Pegawai_model->postCatatanParentUser($data)>0){
                        $j = $hasil_client = 0;
                        for ($i = 0; $i < $jumlah_client; $i++) {
                            $data_client = [
                                'PARENTCATATANID' => $parentid,
                                'EMPLOYEEID' => $clientid[$j],
                                'TANGGALPENGERJAANMULAI' => $this->post('TANGGALPENGERJAANMULAI'),
                                'TANGGALPENGERJAANSELESAI' => $this->post('TANGGALPENGERJAANSELESAI'),
                                'SUBJEKCHILD' => $subjekchild[$j],
                                'TASKCHILD' => $taskchild[$j],
                                'ATTACHMENT' => $this->post('ATTACHMENT')
                            ];
                            $j++;
                            if($this->Pegawai_model->postCatatanClientUser($data_client)>0){
                                $hasil_client++;   
                            }   
                        }
                        if($hasil_client = $jumlah_client){
                            $this->response([
                                'status' => true,
                                'message' => 'Created!'
                            ], REST_Controller::HTTP_CREATED);  
                            }else{
                                $this->response([
                                    'status' => false,
                                    'message' => 'Failed Client!'
                                ], REST_Controller::HTTP_BAD_REQUEST);  
                            }
                    }else{
                        $this->response([
                            'status' => false,
                            'message' => 'Failed Parent!'
                        ], REST_Controller::HTTP_BAD_REQUEST);  
                    }
                }
            }    
        }else{
            $this->response([
                   'status' => FALSE,
                   'message' => 'Token Auth Failed'
                ], REST_Controller::HTTP_NOT_FOUND);
        }
        
    }

    public function index_put()
    {
     
        $id = $this->put('CATATANID');
        $data = [
            'EMPLOYEEID' => $this->post('EMPLOYEEID'),
            'TAHUN' => $this->post('TAHUN'),
            'TANGGAL' => $this->post('TANGGAL'),
            'JAM_MULAI' => $this->post('JAM_MULAI'),
            'JAM_SELESAI' => $this->post('JAM_SELESAI'),
            'SUBJEK' => $this->post('SUBJEK'),
            'ISICATATAN' => $this->post('ISICATATAN'),
            'ATTACHMENT' => $this->post('ATTACHMENT'),
            'JENIS' => $this->post('JENIS'),
            'STATUS' => $this->post('STATUS')
        ];
        if($this->Pegawai_model->putCatatanUser($data, $id) > 0){
            $this->response([
                'status' => true,
                'message' => 'Updated!'
            ], REST_Controller::HTTP_OK);   
        }else{
            $this->response([
                'status' => false,
                'message' => 'Failed to update!'
            ], REST_Controller::HTTP_BAD_REQUEST);  
        }
        
    }

    public function index_delete()
    {
        $parentcatatan = $this->delete('PARENTCATATAN');
        $id = $this->delete('CATATANID');
        if($id === null){
            $this->response([
                'status' => false,
                'message' => 'provide an ID!'
            ], REST_Controller::HTTP_NOT_FOUND);    
        }else {
            if($parentcatatan == 0){
                if($this->Pegawai_model->deleteCatatanUser($id)>0){
                    $this->response([
                        'status' => true,
                        'kode_catatan' => $id,
                        'message' => 'deleted!'
                    ], REST_Controller::HTTP_OK);   
                    }else {
                    $this->response([
                        'status' => false,
                        'message' => 'id not found!'
                    ], REST_Controller::HTTP_BAD_REQUEST);  
                }    
            }else{
                $catatan = $this->Pegawai_model->get1CatatanParentUser($id);
                if(isset($catatan)){
                    $parentcatatanid = $catatan[0]['CATATANID'];
                    if($this->Pegawai_model->deleteCatatanParentUser($id)>0){
                        if($this->Pegawai_model->deleteCatatanChildUser($parentcatatanid)>0){
                            $this->response([
                                'status' => true,
                                'kode_catatan' => $id,
                                'message' => 'deleted!'
                            ], REST_Controller::HTTP_OK);   
                        }else {
                            $this->response([
                                'status' => false,
                                'message' => 'Fail to delete child!'
                            ], REST_Controller::HTTP_BAD_REQUEST);  
                        }   
                    }else {
                        $this->response([
                            'status' => false,
                            'message' => 'Fail to delete parent!'
                        ], REST_Controller::HTTP_BAD_REQUEST);  
                    }
                }else {
                    $this->response([
                         'status' => false,
                         'message' => 'Fail to delete parent(VIEW)!'
                    ], REST_Controller::HTTP_BAD_REQUEST);  
                }
                    
                }
            }
    }


    public function check_flagprioritas_personal($tanggalRencanaMulai)
    {
      // get data from database based on user input
      $flagPrioritasList = $this->Pegawai_model->cekFlagPrioritas($tanggalRencanaMulai);


      if(isset($flagPrioritasList)){
        // Token accepted
        return true;
      }else{
        // Token failed
        return false;
      }
    }

    public function check_flagprioritas_unit($tanggalRencanaMulai)
    {
      // get data from database based on user input
      $flagPrioritasList = $this->Pegawai_model->cekFlagPrioritasUnit($tanggalRencanaMulai);

      if(isset($flagPrioritasList)){
        // Token accepted
        return true;
      }else{
        // Token failed
        return false;

      }
    }

    public function check_token($access_token)
    {
        $where = array(
        'key' => $access_token
      );

      // get data from database based on user input
      $token = $this->Pegawai_model->cekToken($where);

      if(isset($token)){
        // Token accepted
        return true;
      }else{
        // Token failed
        return false;

      }
    }

}
