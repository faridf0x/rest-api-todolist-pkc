<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
use GuzzleHttp\Client;
require APPPATH . 'libraries/REST_Controller.php';

class Auth extends REST_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('Pegawai_model');
        $this->load->model('Auth_model');
    }

    public function index_get()
    {
        $employeeid = $this->get('EMPLOYEEID');
        $password = $this->get('PASSWORD');
        // $tahun = $this->get('tahun');
        
        if (!isset($employeeid) OR !isset($password))
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'USERNAME OR PASSWORD NULL',
                'data' => null
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
        else
        {
          $client = new Client();

          $response = $client->request('GET', 'http://rest.fox.my.id/api/auth', [
            'query' => [
              'EMPLOYEEID' => $employeeid,
              'PASSWORD' => $password
            ]
          ]);

          $result = json_decode($response->getBody()->getContents(), true);

          if($result['status'] === true){

            $access_token = $result['data']['access_token'];
            
                $data = [
                    
                    'user_id' => $employeeid,
                    'key' => $access_token,
                    'level' => '',
                    'ignore_limits' => '',
                    'is_private_key' => '',
                    'ip_addresses' => '',
                    'date_created' => ''
                ];

                if($this->Auth_model->postKey($data)>0){
                    $buat_key = true;
                }else{
                    $buat_key = false;
                }
            
            $this->response([
                'status' => TRUE,
                'message' => 'Auth Berhasil',
                'buat_key' => $buat_key,
                'data' => $result['data']
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
          }else{
            $this->response([
                'status' => FALSE,
                'message' => 'USERNAME OR PASSWORD WRONG',
                'buat_key' => FALSE,
                'data' => null
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
          }
          }
    }



}
