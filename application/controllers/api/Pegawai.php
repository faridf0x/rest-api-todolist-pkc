<?php
 defined('BASEPATH') OR exit('No direct script access allowed');
 use Restserver\Libraries\REST_Controller;
 require APPPATH . 'libraries/REST_Controller.php';
 
 class Pegawai extends REST_Controller {
 	public function __construct(){
 		parent::__construct();
 		$this->load->model('Pegawai_model');
 	} 

	public function index_get()
	{
		$employeeid = $this->get('EMPLOYEEID');
		$access_token = $this->get('access_token');
 		
 		$token_status = $this->check_token($access_token);

 		if($token_status){
 			$data = $this->Pegawai_model->getPegawai($employeeid);
              if($data['PARENTPERSON'] == 0){
                $parentperson = false;
              }else if($data['PARENTPERSON'] == 1){
                $parentperson = true;
              }
              $pegawai = array(
                'access_token' => $data['key'],
                'EMPLOYEEID' => $data['EMPLOYEEID'],
                'EMPLOYEENAME' => $data['EMPLOYEENAME'],
                'EMPLOYEEPICTURE' => $data['EMPLOYEEPICTURE'],
                'PARENTID' => $data['PARENTID'],
                'PARENTNAME' => $data['PARENTNAME'],
                'PARENTPERSON' => $parentperson
              );

              $where = array(
                'PARENTID' => $pegawai['PARENTID']
              );

              $staff = $this->Pegawai_model->getStaff($employeeid);
              $jumlahStaff = count($staff);
              $j=0;
              if($parentperson == true)
              {
                for ($i = 0; $i < $jumlahStaff; $i++) {
                // Looping untuk
                // Memasukkan Data staff menjadi Objek (Saat Login Client)
                // Data Nik SAP / nomor badge
                // Data Nama Lengkap
                // dan Data Nama Superior / Jabatan

                $pegawai['CHILDRENS'][$j]{'EMPLOYEEID'} = $staff[$j]['EMPLOYEEID'];
                $pegawai['CHILDRENS'][$j]{'EMPLOYEENAME'} = $staff[$j]['EMPLOYEENAME'];
                $pegawai['CHILDRENS'][$j]{'EMPLOYEEPICTURE'} = $staff[$j]['EMPLOYEEPICTURE'];
                $j++;

                }
              }else{
                $pegawai['CHILDRENS'] = null;
              }

              // Memasukkan data Catatan menjadi Objek (Saat Login Client)
              // if(isset($tahun)){
              //   $catatan= $this->Pegawai_model->getCatatanUser($nik_sap, $tahun);
              //   $jumlahCatatan = count($catatan);
              //   if($jumlahCatatan > 0)
              //   {
              //   $j = 0;
              //   for ($i = 0; $i < $jumlahCatatan; $i++){
              //     $pegawai['catatan'][$j] = $catatan[$j];
              //     $j++;
              //     }
              //   }else{
              //     $pegawai['catatan'] = null;
              //   }
              // }
              
              $this->response([
                  'status' => TRUE,
                  'message' => 'authentication successful',
                  'data' => $pegawai,
              ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code

              	if($pegawai){
		 			$this->response([
		 				'status' => true,
		 				'message' => 'data found',
		 				'data' => $pegawai
		 			], REST_Controller::HTTP_OK);
		 		}else{
		 			$this->response([
		 				'status' => false,
		 				'message' => 'id not found',
		 				'data' => null
		 			], REST_Controller::HTTP_NOT_FOUND);
		 		}
 		}else{
 			$this->response([
		 				'status' => false,
		 				'message' => 'id OR access_token null',
		 				'data' => null
		 			], REST_Controller::HTTP_NOT_FOUND);
 		}

 		
	}

	public function index_post()
	{
		
	}

	public function index_put()
	{
		$id = $this->put('nik_sap');
	 	$data = [ 
	 		'password' => md5($this->put('password'))
	 	];
 		if($this->Pegawai_model->putPasswordUser($data, $id) > 0){
 			$this->response([
 				'status' => true,
 				'message' => 'Updated!'
 			], REST_Controller::HTTP_OK);	
 		}else{
 			$this->response([
 				'status' => false,
 				'message' => 'Failed to update!'
 			], REST_Controller::HTTP_BAD_REQUEST);	
 		}
	}

	public function index_delete()
	{
		
	}

	public function check_token($access_token)
	{
		$where = array(
        'key' => $access_token
      );

      // get data from database based on user input
      $token = $this->Pegawai_model->cekToken($where);

      if(isset($token)){
        // Token accepted
        return true;
      } else {
        // Token failed
        return false;

      }
	}

}

/* End of file Pegawai.php */
/* Location: ./application/controllers/api/Pegawai.php */