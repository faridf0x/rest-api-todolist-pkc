<?php
 use Restserver\Libraries\REST_Controller;
 defined('BASEPATH') OR exit('No direct script access allowed');

 require APPPATH . 'libraries/REST_Controller.php';
 
 class PegawaiAdminPKC extends REST_Controller {
 	public function __construct(){
 		parent::__construct();
 		$this->load->model('Pegawai_model');
 	} 

	public function index_get()
	{
		$id = $this->get('s_id_PKCAdmin');
		$usertype = $this->get('usertype');

		if($usertype === 'adminPKC-TI'){
			if($id == null){
	 			$pegawai = $this->Pegawai_model->getPegawai();
	 			$pegawaiStaff = "";
	 		}else{
	 			$data = $this->Pegawai_model->getPegawai($id);

	 			$pegawai = array(
	              'nik_sap' => $data['nik_sap'],
	              'nama_lengkap' => $data['nama_lengkap'],
	              'id_unit' => $data['id_unit'],
	              'id_dep' => $data['id_dep'],
	              'nama_dep' => $data['nama_dep'],
	              'id_sup' => $data['id_sup'],
	              'nama_sup' => $data['nama_sup']
	            );	

	 			$staff = $this->Pegawai_model->getStaff($id);

	 			$jumlahStaff = count($staff);
	 			$j=0;
	 			if($jumlahStaff > 0)
	            {
	               for ($i = 0; $i < $jumlahStaff; $i++) {
	               // Looping untuk
	               // Memasukkan Data staff menjadi Objek (Saat Login Client)
	               // Data Nik SAP / nomor badge
	               // Data Nama Lengkap
	               // dan Data Nama Superior / Jabatan

	               $pegawai['staff'][$j]{'nik_sap'} = $staff[$j]['staff'];
	               $data_staff = $this->Pegawai_model->getDataStaff($staff[$j]['staff']);
	               $pegawai['staff'][$j]{'nama_staff'} = $data_staff['nama_lengkap'];
	               $sup_staff = $this->Pegawai_model->getSupStaff($data_staff['id_sup']);
	               $pegawai['staff'][$j]{'sup_staff'} = $sup_staff{'nama_sup'};
	               $j++;

	              }
	            }else{
	              $pegawai['staff'] = null;
	            }

	 		}
		}else{
			$this->response([
 				'status' => false,
 				'message' => 'you are not have access',
 				'data' => null
 			], REST_Controller::HTTP_NOT_FOUND);
		}

 		// if($id != null){
 		// 	$mahasiswa = $this->Mahasiswa_model->getMahasiswa($id);
 		// }
 		// if($id_cat != null){
 		// 	$catatan = $this->Mahasiswa_model->getCatatan($id_cat); 
 		// }	 

 		if($pegawai){
 			$this->response([
 				'status' => true,
 				'message' => 'data found',
 				'data' => $pegawai
 			], REST_Controller::HTTP_OK);
 		}else{
 			$this->response([
 				'status' => false,
 				'message' => 'id not found',
 				'data' => null
 			], REST_Controller::HTTP_OK);
 		}
	}

	public function index_post()
	{
		$id = $this->get('s_id');
		$usertype = $this->get('usertype');

		
		$data = [
 			'nik_sap' => $this->post('nik_sap'),
 			'nama_lengkap' => $this->post('nama_lengkap'),
 			'id_unit' => $this->post('id_unit'),
 			'id_dep' => $this->post('id_dep'),
			'id_sup' => $this->post('id_sup')
 		];

 		if($this->Pegawai_model->postPegawai($data)>0){
	 		$this->response([
	 			'status' => true,
	 			'message' => 'Created!'
	 		], REST_Controller::HTTP_CREATED);	
	 	}else{
	 		$this->response([
	 			'status' => false,
	 			'message' => 'Failed!'
	 		], REST_Controller::HTTP_BAD_REQUEST);	
	 	}
	}

	public function index_put()
	{
		$id = $this->put('nik_sap');
	 	$data = [ 
	 		'password' => '6d081fc4b469dfc2682b2d80c5a19331'
	 	];
 		if($this->Pegawai_model->putPasswordUser($data, $id) > 0){
 			$this->response([
 				'status' => true,
 				'message' => 'Updated!'
 			], REST_Controller::HTTP_OK);	
 		}else{
 			$this->response([
 				'status' => false,
 				'message' => 'Failed to update!'
 			], REST_Controller::HTTP_BAD_REQUEST);	
 		}
 		
	}

	public function index_delete()
	{
		
	}

}

/* End of file Pegawai.php */
/* Location: ./application/controllers/api/Pegawai.php */