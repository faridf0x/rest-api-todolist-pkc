<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
require APPPATH . 'libraries/REST_Controller.php';


class CatatanUnit extends REST_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('Pegawai_model');
    }

    public function index_get()
    {
        $employeeid = $this->get('EMPLOYEEID');
        $tahun = $this->get('TAHUN');
        $access_token = $this->get('access_token');
        $parentperson = $this->get('PARENTPERSON');
        $parentid = $this->get('PARENTPERSONID');

        $token_status = $this->check_token($access_token);

        if($token_status){
            if($parentperson == true){
                $this->getCatatanUnit($employeeid, $parentperson, $parentid, $tahun);
            }
            
        }else {
            $this->response([
                'status' => FALSE,
                'message' => 'Token Auth Failed'
            ], REST_Controller::HTTP_OK);
        }
        
    }    

    public function getCatatanUnit($employeeid, $parentperson, $parentid, $tahun)
    {
      if ($employeeid == null OR $tahun == null)
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Login OR tahun were not found'
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
        else
        {

              $staff = $this->Pegawai_model->getStaff($employeeid);
              $jumlahStaff = count($staff);
              $j=0;
              if($parentperson == true)
              {
                for ($i = 0; $i < $jumlahStaff; $i++) {
                // Looping untuk
                // Memasukkan Data staff menjadi Objek (Saat Login Client)
                // Data Nik SAP / nomor badge
                // Data Nama Lengkap
                // dan Data Nama Superior / Jabatan

                $pegawaiid[$j] = $staff[$j]['EMPLOYEEID'];
                $catatan[$j] = $this->Pegawai_model->getCatatanUser($pegawaiid[$j], $tahun);
                $j++;

                }

              }else{
                $pegawai = null;
              }
            
            $this->response([
              'status' => true,
              'message' => 'data found',
              'data' => $catatan
            ], REST_Controller::HTTP_OK);   
        }
    }

    public function index_post()
    {
        $access_token = $this->post('access_token');

        $token_status = $this->check_token($access_token);

        if($token_status){
            $data = [
                'EMPLOYEEID' => $this->post('EMPLOYEEID'),
                'TAHUN' => $this->post('TAHUN'),
                'TANGGAL' => $this->post('TANGGAL'),
                'JAM_MULAI' => $this->post('JAM_MULAI'),
                'JAM_SELESAI' => $this->post('JAM_SELESAI'),
                'SUBJEK' => $this->post('SUBJEK'),
                'ISICATATAN' => $this->post('ISICATATAN'),
                'ATTACHMENT' => $this->post('ATTACHMENT'),
                'JENIS' => $this->post('JENIS'),
                'STATUS' => $this->post('STATUS')
            ];

            if($this->Pegawai_model->postCatatanUser($data)>0){
                $this->response([
                    'status' => true,
                    'message' => 'Created!'
                ], REST_Controller::HTTP_CREATED);  
            }else{
                $this->response([
                    'status' => false,
                    'message' => 'Failed!'
                ], REST_Controller::HTTP_BAD_REQUEST);  
            }
        }else{
            $this->response([
                'status' => FALSE,
                'message' => 'Token Auth Failed'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function index_put()
    {
     
        $id = $this->put('CATATANID');
        $data = [
            'EMPLOYEEID' => $this->post('EMPLOYEEID'),
            'TAHUN' => $this->post('TAHUN'),
            'TANGGAL' => $this->post('TANGGAL'),
            'JAM_MULAI' => $this->post('JAM_MULAI'),
            'JAM_SELESAI' => $this->post('JAM_SELESAI'),
            'SUBJEK' => $this->post('SUBJEK'),
            'ISICATATAN' => $this->post('ISICATATAN'),
            'ATTACHMENT' => $this->post('ATTACHMENT'),
            'JENIS' => $this->post('JENIS'),
            'STATUS' => $this->post('STATUS')
        ];
        if($this->Pegawai_model->putCatatanUser($data, $id) > 0){
            $this->response([
                'status' => true,
                'message' => 'Updated!'
            ], REST_Controller::HTTP_OK);   
        }else{
            $this->response([
                'status' => false,
                'message' => 'Failed to update!'
            ], REST_Controller::HTTP_BAD_REQUEST);  
        }
        
    }

    public function index_delete()
    {
        $id = $this->delete('CATATANID');
        if($id === null){
            $this->response([
                'status' => false,
                'message' => 'provide an ID!'
            ], REST_Controller::HTTP_NOT_FOUND);    
        }else {
            if($this->Pegawai_model->deleteCatatanUser($id)>0){
            $this->response([
                'status' => true,
                'kode_catatan' => $id,
                'message' => 'deleted!'
            ], REST_Controller::HTTP_OK);   
            }else {
            $this->response([
                'status' => false,
                'message' => 'id not found!'
            ], REST_Controller::HTTP_BAD_REQUEST);  
            }
        }
    }

    public function check_token($access_token)
    {
        $where = array(
        'key' => $access_token
      );

      // get data from database based on user input
      $token = $this->Pegawai_model->cekToken($where);

      if(isset($token)){
        // Token accepted
        return true;
      }else{
        // Token failed
        return false;

      }
    }

}
