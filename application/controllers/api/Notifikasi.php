<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
require APPPATH . 'libraries/REST_Controller.php';


class Notifikasi extends REST_Controller {
	function __construct()
    {
        parent::__construct();
        $this->load->model('Pegawai_model');
    }

    public function index_get()
    {
        $nik_sap = $this->get('nik_sap');

        $this->getNotifikasi($nik_sap);
    }

    public function getNotifikasi($nik_sap){
    	if ($nik_sap == null)
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Not Login'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
        else
        {
            $notifikasi = $this->Pegawai_model->getNotifikasiUser($nik_sap);
            $this->response([
              'status' => true,
              'message' => 'data found',
 			  'data' => $notifikasi
            ], REST_Controller::HTTP_OK);   
        }
    }

    public function index_post()
    {
        $data = [
            'nik_sap' => $this->post('nik_sap'),
            'kode_catatan' => $this->post('kode_catatan'),
            'notifikasi' => $this->post('notifikasi')
        ];

        if($this->Pegawai_model->postNotifikasiUser($data)>0){
            $this->response([
                'status' => true,
                'message' => 'Created!'
            ], REST_Controller::HTTP_CREATED);  
        }else{
            $this->response([
                'status' => false,
                'message' => 'Failed!'
            ], REST_Controller::HTTP_BAD_REQUEST);  
        }
    }

    public function index_delete()
    {
        $id = $this->delete('nik_sap');
        if($id === null){
            $this->response([
                'status' => false,
                'message' => 'provide an ID!'
            ], REST_Controller::HTTP_NOT_FOUND);    
        }else {
            if($this->Pegawai_model->deleteNotifikasiUser($id)>0){
            $this->response([
                'status' => true,
                'nik_sap' => $id,
                'message' => 'deleted!'
            ], REST_Controller::HTTP_OK);   
            }else {
            $this->response([
                'status' => false,
                'message' => 'id not found!'
            ], REST_Controller::HTTP_BAD_REQUEST);  
            }
        }
    }
}