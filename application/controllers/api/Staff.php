<?php
 defined('BASEPATH') OR exit('No direct script access allowed');
 use Restserver\Libraries\REST_Controller;
 require APPPATH . 'libraries/REST_Controller.php';
 
 class Staff extends REST_Controller {
 	public function __construct(){
 		parent::__construct();
 		$this->load->model('Pegawai_model');
 	} 

	public function index_get()
	{
		$employeeid = $this->get('EMPLOYEEID');
		$parentperson = $this->get('PARENTPERSON');
		$access_token = $this->get('access_token');

		
 		$token_status = $this->check_token($access_token);

 		if($token_status){
			$staff = $this->Pegawai_model->getStaff($employeeid);
            $jumlahStaff = count($staff);
            $j=0;
            if($parentperson == true)
            {
                for ($i = 0; $i < $jumlahStaff; $i++) {
                // Looping untuk

                $pegawai['CHILDRENS'][$j]{'EMPLOYEEID'} = $staff[$j]['EMPLOYEEID'];
                $pegawai['CHILDRENS'][$j]{'EMPLOYEENAME'} = $staff[$j]['EMPLOYEENAME'];
                $pegawai['CHILDRENS'][$j]{'EMPLOYEEPICTURE'} = $staff[$j]['EMPLOYEEPICTURE'];
                $j++;

                }
            }else{
                $pegawai = null;
            }

            
            if($pegawai){
		 		$this->response([
		 			'status' => true,
		 			'message' => 'get staff successful',
		 			'data' => $pegawai
		 		], REST_Controller::HTTP_OK);
		 	}else{
		 		$this->response([
		 			'status' => false,
		 			'message' => 'staff not found',
		 			'data' => null
		 		], REST_Controller::HTTP_NOT_FOUND);
		 	}
        }else{
        	$this->response([
				'status' => false,
				'message' => 'id OR access_token null',
				'data' => null
			], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function check_token($access_token)
	{
		$where = array(
        'key' => $access_token
      );

      // get data from database based on user input
      $token = $this->Pegawai_model->cekToken($where);

      if(isset($token)){
        // Token accepted
        return true;
      }else{
        // Token failed
        return false;

      }
	}

}