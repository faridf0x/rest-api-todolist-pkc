<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model {

	public function login($where)
	{		
		return $this->db->get_where('tb_pegawai', $where)->row_array();
	}
	public function postKey($data){
		$this->db->insert('keys', $data);
		return $this->db->affected_rows();
	}

}

/* End of file Auth_model.php */
/* Location: ./application/models/Auth_model.php */