<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai_model extends CI_Model {

	//Awal Model untuk Admin

	public function getToken($id = null){
		$this->db->select('key');
		$this->db->from('keys');
		$this->db->join('keys', 'keys.user_id = 1');
		$this->db->where('tb_pegawai.EMPLOYEEID', '1');
		return $this->db->get()->row_array();
	}

	public function getPegawai($id=null)
	{
		$this->db->select('*','keys');
		$this->db->from('tb_pegawai');
		$this->db->join('keys', 'keys.user_id = 1');
		$this->db->where('tb_pegawai.EMPLOYEEID', $id);
		return $this->db->get()->row_array();
	}

	public function getStaff($id){
		$this->db->select('*');
		$this->db->from('tb_pegawai');
		return $this->db->get()->result_array();
	}

	public function getKodeStaff($id){
			$this->db->select('staff');
			return $this->db->get_where('tb_staff', array('nik_sap' => $id))->row_array();
	}
	
	public function getSupStaff($id){
			return $this->db->get_where('tb_superior', array('id_sup' => $id))->row_array();
	}

	public function postPegawai($data){
		$this->db->insert('tb_pegawai', $data);
		return $this->db->affected_rows();
	}

	public function putPegawai($data)
	{
		$this->db->update('tb_pegawai', $data, ['nik_sap' => $id]);
		return $this->db->affected_rows();
	}

	public function deletePegawai($data, $id){
		$this->db->delete('tb_pegawai', $data, ['nik_sap' => $id]);
		return $this->db->affected_rows();
	}

	//Akhir Model untuk Admin

	//Awal Model untuk Client
	public function getCatatanUserAll($id, $tahun){
		// $this->db->get_where('tb_catatan', ['nik_sap'=>$id, 'tanggal' => $tanggal]);
		$where = array('TAHUN' => $tahun,);
		$this->db->order_by('TANGGALRENCANAMULAI', 'DESC');
		$this->db->select('*');
		$this->db->from('tb_catatan');
		$this->db->where($where);
		return $this->db->get()->result_array();
	}

	public function getCatatanUser($id, $tahun, $flag=null){
		// $this->db->get_where('tb_catatan', ['nik_sap'=>$id, 'tanggal' => $tanggal]);
		$where = array('EMPLOYEEID' => $id, 'TAHUN' => $tahun, 'FLAGPROGRESS' => $flag);
		$this->db->order_by('TANGGALRENCANAMULAI', 'DESC');
		$this->db->select('*');
		$this->db->from('tb_catatan');
		$this->db->where($where);
		return $this->db->get()->result_array();
	}
	public function getLastCatatanUser($id){
		// $this->db->get_where('tb_catatan', ['nik_sap'=>$id, 'tanggal' => $tanggal]);
		$where = array('EMPLOYEEID' => $id);
		$this->db->order_by('CATATANID', 'DESC');
		$this->db->select('CATATANID');
		$this->db->from('tb_parent_catatan');
		$this->db->limit(1);
		$this->db->where($where);
		return $this->db->get()->row_array();
	}

	public function getCatatanParentUser($id, $tahun, $flagprogress){
		// $this->db->get_where('tb_catatan', ['nik_sap'=>$id, 'tanggal' => $tanggal]);
		$where = array('EMPLOYEEID' => $id, 'TAHUN' => $tahun, 'FLAGPROGRESS' => $flagprogress);
		$this->db->order_by('TANGGALRENCANAMULAI', 'DESC');
		$this->db->select('*');
		$this->db->from('tb_parent_catatan');
		$this->db->where($where);
		return $this->db->get()->result_array();
	}

	public function getCatatanParentUserAll($id, $tahun){
		// $this->db->get_where('tb_catatan', ['nik_sap'=>$id, 'tanggal' => $tanggal]);
		$where = array('TAHUN' => $tahun);
		$this->db->order_by('TANGGALRENCANAMULAI', 'DESC');
		$this->db->select('*');
		$this->db->from('tb_parent_catatan');
		$this->db->where($where);
		return $this->db->get()->result_array();
	}

	public function getSubjekCatatanParentUser($id){
		// $this->db->get_where('tb_catatan', ['nik_sap'=>$id, 'tanggal' => $tanggal]);
		$where = array('EMPLOYEEID' => $id);
		$this->db->order_by('SUBJEK', 'ASC');
		$this->db->select('*');
		$this->db->from('tb_parent_catatan');
		$this->db->where($where);
		return $this->db->get()->result_array();
	}

	public function getCatatanChildUser($id, $tahun){
		// $this->db->get_where('tb_catatan', ['nik_sap'=>$id, 'tanggal' => $tanggal]);
		$where = array('EMPLOYEEID' => $id);
		$this->db->order_by('ID', 'DESC');
		$this->db->select('*');
		$this->db->from('tb_child_catatan');
		$this->db->where($where);
		return $this->db->get()->result_array();
	}

	public function getCatatanChildGetParentUser($parentcatatanid){
		// $this->db->get_where('tb_catatan', ['nik_sap'=>$id, 'tanggal' => $tanggal]);
		$where = array('CATATANID' => $parentcatatanid);
		$this->db->select('*');
		$this->db->from('tb_parent_catatan');
		$this->db->where($where);
		return $this->db->get()->row_array();
	}

	public function getCatatanParentChildUser($parentcatatanid){
		// $this->db->get_where('tb_catatan', ['nik_sap'=>$id, 'tanggal' => $tanggal]);
		$where = array('PARENTCATATANID' => $parentcatatanid);
		$this->db->order_by('ID', 'DESC');
		$this->db->select('*');
		$this->db->from('tb_child_catatan');
		$this->db->where($where);
		return $this->db->get()->result_array();
	}

	public function get1CatatanParentUser($id){
		// $this->db->get_where('tb_catatan', ['nik_sap'=>$id, 'tanggal' => $tanggal]);
		$where = array('CATATANID' => $id);
		$this->db->order_by('TANGGALRENCANAMULAI', 'DESC');
		$this->db->select('*');
		$this->db->from('tb_parent_catatan');
		$this->db->where($where);
		return $this->db->get()->result_array();
	}

	public function getNotifikasiUser($id){
		$where = array('nik_sap' => $id);
		$this->db->select('*');
		$this->db->from('tb_notifikasi');
		$this->db->where($where);
		return $this->db->get()->result_array();
	}

	public function postCatatanUser($data){
		$this->db->insert('tb_catatan', $data);
		return $this->db->affected_rows();
	}

	public function postCatatanParentUser($data){
		$this->db->insert('tb_parent_catatan', $data);
		return $this->db->affected_rows();
	}

	public function postCatatanClientUser($data){
		$this->db->insert('tb_child_catatan', $data);
		return $this->db->affected_rows();
	}

	public function postParentCatatan($data){
		$this->db->insert('tb_parent_catatan', $data);
		return $this->db->affected_rows();
	}

	public function postNotifikasiUser($data){
		$this->db->insert('tb_notifikasi', $data);
		return $this->db->affected_rows();
	}

	public function putCatatanUser($data, $id)
	{
		$this->db->update('tb_catatan', $data, ['CATATANID' => $id]);
		return $this->db->affected_rows();
	}

	public function putPasswordUser($data, $id)
	{
		$this->db->update('tb_pegawai', $data, ['nik_sap' => $id]);
		return $this->db->affected_rows();
	}

	public function putProgressCatatanPersonal($data, $id)
	{
		$this->db->update('tb_catatan', $data, ['CATATANID' => $id]);
		return $this->db->affected_rows();
	}

	public function putProgressCatatanChild($data, $where)
	{
		$this->db->update('tb_child_catatan', $data, $where);
		return $this->db->affected_rows();
	}

	public function putAccCatatanPersonal($data, $where)
	{
		$this->db->update('tb_catatan', $data, $where);
		return $this->db->affected_rows();
	}

	public function putAccCatatanUnit($data, $where)
	{
		$this->db->update('tb_child_catatan', $data, $where);
		return $this->db->affected_rows();
	}

	public function putValidasiCatatan($data, $where)
	{
		$this->db->update('tb_child_catatan', $data, $where);
		return $this->db->affected_rows();
	}

	
	public function putValidasiCatatanParent($data, $where)
	{
		$this->db->update('tb_parent_catatan', $data, $where);
		return $this->db->affected_rows();
	}

	public function deleteCatatanUser($id){
		$this->db->delete('tb_catatan', ['CATATANID' => $id]);
		return $this->db->affected_rows();
	}
	public function deleteCatatanParentUser($id){
		$this->db->delete('tb_parent_catatan', ['CATATANID' => $id]);
		return $this->db->affected_rows();
	}
	public function deleteCatatanChildUser($id){
		$this->db->delete('tb_child_catatan', ['PARENTCATATANID' => $id]);
		return $this->db->affected_rows();
	}
	public function deleteNotifikasiUser($id){
		$this->db->delete('tb_notifikasi', ['nik_sap' => $id]);
		return $this->db->affected_rows();
	}
	//Akhir Model untuk Client

	public function login($where)
	{		
		return $this->db->get_where('tb_pegawai', $where)->row_array();
	}

	public function cekFlagPrioritas($tanggalRencanaMulai)
	{		
		$where = array('TANGGALRENCANAMULAI' => $tanggalRencanaMulai, 'FLAGPRIORITAS' => '1');
		$this->db->like($where);
		return $this->db->get('tb_catatan')->row_array();
	}

	public function cekFlagPrioritasUnit($tanggalRencanaMulai)
	{		
		$where = array('TANGGALRENCANAMULAI' => $tanggalRencanaMulai, 'FLAGPRIORITAS' => '1');
		$this->db->like($where);
		return $this->db->get('tb_parent_catatan')->row_array();
	}

	public function cekToken($where)
	{		
		return $this->db->get_where('keys', $where)->row_array();
	}
}

/* End of file Pegawai_model.php */
/* Location: ./application/models/Pegawai_model.php */